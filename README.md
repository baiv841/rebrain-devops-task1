# rebrain-devops-task1

##### Modify `README.md` and add to the git index

```console
$ git add README.md
```

##### Apply git commit

```console
$ git commit -m "Modify README.md"
```

##### Copy preloaded default `nginx.conf` file to the repository folder

```console
$ cp ~/Downloads/nginx.conf ./nginx.conf
```

##### Add `nginx.conf` to the git index

```console
$ git add nginx.conf
```

##### Apply git commit

```console
$ git commit -m "Add default nginx.conf file"
```

##### Push all changes to the git server

```console
$ git push -uf origin main
```

##### Make sure all commits visible in log

```console
$ git log
```

# Projects goals

This project was written as a study project for DevOps rebrainme.com course [Rebrainme.com](https://rebrainme.com/)
